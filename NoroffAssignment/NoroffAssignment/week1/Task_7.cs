﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NoroffAssignment.week1
{
    class Task_7
    {
        public static void fibonacci()
        {
            List<int> fib = new List<int>();
            fib.Add(1);
            fib.Add(2);
            CreateFib(fib);
            Console.WriteLine($"Sum of the fibonacci even number: {SumEven(fib)}");
        }
        /// <summary>
        /// Generate a fibonacci list of n_i = n_i1 + n_i2 with n_i1 + n_i2 < 4000000 
        /// </summary>
        /// <param name="fib"></param>
        static void CreateFib(List<int> fib)
        {
            if (fib.ElementAt(fib.Count-1) 
                + fib.ElementAt(fib.Count - 2)  > 4000000)
            {
                return;
            }
            fib.Add(fib.ElementAt(fib.Count - 2) + fib.ElementAt(fib.Count - 1));
            CreateFib(fib);
        }


        /// <summary>
        /// Sum all the value in the list that is even and return the total sum.
        /// </summary>
        /// <param name="fib"></param>
        /// <returns></returns>
        static int SumEven(List<int> fib)
        {
            int sum = 0;
            foreach(int i in fib)
            {
                Console.WriteLine(i);
                if (i % 2 == 0) sum += i;
            }
            return sum;
        }
    }
}
